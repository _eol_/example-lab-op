// Example2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

#include "Repair.h"

int main()
{
	Repair r("Halytska", 100);

	Machine katok("Katok", 250);
	Machine katok2 = katok;
	Machine tractor("Traktor", 200);

	r.addMachine(&katok);
	r.addMachine(&katok2);
	r.addMachine(&tractor);
	r.addWorkers(5);

	cout << r.getDescription();

	cout << "--- program end ---\n";
}

