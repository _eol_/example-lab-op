#pragma once
#include <string>
#include <sstream>
#include <vector>

#include "Machine.h"

class Repair
{
	const MONEY workerWage = 100;
	const int workHours = 8;
	const double hoursPerMeter = 0.1;
	const MONEY asfaltCostPerMeter = 100;
	const MONEY adminCost = 10000;

	std::string street;
	double area = 0.0;
	std::vector<Machine*> machines;
	Machine mitla;
	int workers = 0;
	double hours = 0.0;
	MONEY cost = 0;

	void calculate();

public:
	Repair(std::string street_, double area_)
		:street(street_)
		, area(area_)
		, mitla("Mitla", 10)
	{
		calculate();
		std::cout << "Repair constructor: " << street << ", " << area << " m2\n";
	}
	~Repair()
	{
		std::cout << "Repair destructor: " << street << ", " << area << " m2\n";
	}
	int getDays() const { return hours / workHours + 1; }
	MONEY getCost() const { return cost; }
	bool addMachine(Machine* m);
	void addWorkers(int n) { workers += n; calculate(); }
	std::string getDescription() const;
};


