#pragma once

#include <iostream>

typedef int MONEY;

class Machine
{
	std::string name;
	MONEY costPerHour;
	bool inWork = false;
public:
	Machine(std::string n, MONEY cost): name(n), costPerHour(cost)
	{ 
		std::cout << "Machine constructor: " << name << std::endl;
	}
	Machine(const Machine& other) :name(other.name), costPerHour(other.costPerHour)
	{
		std::cout << "Machine copy constructor: " << name << std::endl;
	}
	~Machine()
	{
		std::cout << "Machine destructor: " << name << std::endl;
	}
	MONEY getCost() const { return costPerHour; }
	bool intoWork()
	{
		if (inWork) return false;
		inWork = true;
		return true;
	}
	std::string getDescription() const
	{
		return name + "(" + std::to_string(costPerHour) + " hrn/hour)";
	}
};


