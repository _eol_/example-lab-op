#pragma once


typedef int MONEY;

class Machine
{
	std::string name;
	MONEY costPerHour;
	bool inWork = false;
public:
	void init(std::string n, MONEY cost) { name = n; costPerHour = cost; }
	MONEY getCost() const { return costPerHour; }
	bool intoWork()
	{
		if (inWork) return false;
		inWork = true;
		return true;
	}
	std::string getDescription() const
	{
		return name + "(" + std::to_string(costPerHour) + " hrn/hour)";
	}
};


