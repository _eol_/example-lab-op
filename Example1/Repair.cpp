#include "Repair.h"
using namespace std;

bool Repair::addMachine(Machine* m)
{
	if (m->intoWork())
	{
		machines.push_back(m);
		workers++;
		calculate();
		return true;
	}
	return false;
}

void Repair::calculate()
{
	hours = area * hoursPerMeter;
	MONEY hourCost = workers * workerWage;
	for (Machine* m : machines)
	{
		hourCost += m->getCost();
	}
	cost = adminCost + asfaltCostPerMeter * area + hourCost * hours + mitla.getCost();
}

string Repair::getDescription() const
{
	if (area == 0.0) return "Empty repair\n";

	ostringstream desc;
	desc << "Repair on " << street << ", area=" << area << endl;
	desc << "Duration: " << getDays() << " days\n";
	desc << "Materials: " << asfaltCostPerMeter * area << " hrn\n";
	desc << "Workers: " << workers << endl;
	desc << "Machines: \n";
	for (Machine* m : machines)
	{
		desc << "\t" << m->getDescription();
		desc << ": " << m->getCost() * hours << " hrn\n";
	}
	desc << "Workers' wage: " << workers * workerWage * hours << " hrn\n";
	desc << "Total cost: " << cost << " hrn\n";
	return desc.str();
}

