#include <iostream>
using namespace std;

#include "Repair.h"

int main()
{
    Machine katok;
	katok.init("Katok", 250);
	Machine tractor;
	tractor.init("Traktor", 200);

    Repair r;
	cout << "Empty repair: " << r.getDescription() << endl;
	r.init("Halytska", 100);
    r.addMachine(&katok);
    r.addMachine(&tractor);
    r.addWorkers(5);

	cout << r.getDescription();

}

